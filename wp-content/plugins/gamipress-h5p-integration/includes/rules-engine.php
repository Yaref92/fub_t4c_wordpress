<?php
/**
 * Rules Engine
 *
 * @package GamiPress\H5P\Rules_Engine
 * @since 1.0.0
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Checks if an user is allowed to work on a given requirement related to a content type
 *
 * @since  1.0.0
 *
 * @param bool $return          The default return value
 * @param int $user_id          The given user's ID
 * @param int $requirement_id   The given requirement's post ID
 * @param string $trigger       The trigger triggered
 * @param int $site_id          The site id
 * @param array $args           Arguments of this trigger
 *
 * @return bool True if user has access to the requirement, false otherwise
 */
function gamipress_h5p_user_has_access_to_achievement( $return = false, $user_id = 0, $requirement_id = 0, $trigger = '', $site_id = 0, $args = array() ) {

    // If we're not working with a requirement, bail here
    if ( ! in_array( get_post_type( $requirement_id ), gamipress_get_requirement_types_slugs() ) )
        return $return;

    // Check if user has access to the achievement ($return will be false if user has exceed the limit or achievement is not published yet)
    if( ! $return )
        return $return;

    // If is specific content type trigger, rules engine needs to check the content type
    if( $trigger === 'gamipress_h5p_complete_specific_content_type'
        || $trigger === 'gamipress_h5p_max_complete_specific_content_type'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_min_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_max_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_between_score' ) {

        $content_type = $args[3];

        $required_content_type = get_post_meta( $requirement_id, '_gamipress_h5p_content_type', true );

        // True if content type is the required content type
        $return = (bool) ( $content_type === $required_content_type );
    }

    // If is minimum score trigger, rules engine needs to check the minimum score
    if( $return && ( $trigger === 'gamipress_h5p_complete_content_min_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_min_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_min_score' ) ) {

        $score = absint( $args[4] );

        $required_score = absint( get_post_meta( $requirement_id, '_gamipress_h5p_score', true ) );

        // True if there is score is bigger than required score
        $return = (bool) ( $score >= $required_score );
    }

    // If is maximum score trigger, rules engine needs to check the maximum score
    if( $return && ( $trigger === 'gamipress_h5p_complete_content_max_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_max_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_max_score' ) ) {

        $score = absint( $args[4] );

        $required_score = absint( get_post_meta( $requirement_id, '_gamipress_h5p_score', true ) );

        // True if there is score is lower than required score
        $return = (bool) ( $score <= $required_score );
    }

    // If is between score trigger, rules engine needs to check if score is between range of scores
    if( $return && ( $trigger === 'gamipress_h5p_complete_content_between_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_between_score'
        || $trigger === 'gamipress_h5p_complete_specific_content_type_between_score' ) ) {

        $score = absint( $args[4] );

        $min_score = absint( get_post_meta( $requirement_id, '_gamipress_h5p_min_score', true ) );
        $max_score = absint( get_post_meta( $requirement_id, '_gamipress_h5p_max_score', true ) );

        // True if there is score is bigger than min score and lower than max score
        $return = (bool) ( $score >= $min_score && $score <= $max_score );
    }

    // Send back our eligibility
    return $return;

}
add_filter( 'user_has_access_to_achievement', 'gamipress_h5p_user_has_access_to_achievement', 10, 6 );